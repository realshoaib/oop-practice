﻿using System;

namespace BankAccount
{
    class Program
    {
        static void Main(string[] args)
        {
            bool keepLooping = true;

            FirstPrompt();

            while (keepLooping)
            {
                NewAccount();
            }

            void FirstPrompt()
            {
                Console.WriteLine("Do you want to create a new account? (Y/N): ");
                var x = Console.ReadLine();

                if (x == "Y" || x == "y")
                {
                    NewAccount();
                }
                else
                {
                    Console.WriteLine("You should give an appropriate input if you really want to test the application...!! \n");
                    DefaultAccount();
                }
            }

            void NewAccount()
            {
                Console.WriteLine("Which type of account you want to create? S for Saving, C for Credit, G for GiftCard Account & E to Exit!");
                var a = Console.ReadLine();

                if (a == "g" || a == "G")
                {
                    GiftCardAccount();
                }
                else if (a == "s" || a == "S")
                {
                    SavingAccount();
                }
                else if (a == "c" || a == "C")
                {
                    CreditAccount();
                }
                else if (a == "e" || a == "E")
                {
                    keepLooping = false;
                }
                else
                {
                    Console.WriteLine("Give an appropriate input...!! \n");
                    NewAccount();
                }
            }

            void SavingAccount()
            {
                Console.WriteLine("-------------Saving Account-------------");
                Console.WriteLine("Enter your Name?");
                var name = Console.ReadLine();
                Console.WriteLine("Enter Initial Deposit?");
                var initialDeposit = Convert.ToInt32(Console.ReadLine());
                var savingAccount = new InterestEarningAccount(name, initialDeposit);
                Console.WriteLine($"Saving Account No {savingAccount.Number} was created for {savingAccount.Owner} with an initial balance of {savingAccount.Balance}$, Enjoy transactions!");
            YES:
                CommitTransactions(savingAccount);
                goto YES;
            }
            void CreditAccount()
            {
                Console.WriteLine("-------------Line of Credit Account-------------");
                Console.WriteLine("Enter your Name?");
                var name = Console.ReadLine();
                Console.WriteLine("Enter Initial Deposit?");
                var initialDeposit = Convert.ToInt32(Console.ReadLine());
                var creditAccount = new LineOfCreditAccount(name, 0, initialDeposit);
                Console.WriteLine($"Credit Account No {creditAccount.Number} was created for {creditAccount.Owner} with an initial balance of {creditAccount.Balance}$, Enjoy transactions!");
            YES:
                CommitTransactions(creditAccount);
                goto YES;
            }
            void GiftCardAccount()
            {
                Console.WriteLine("-------------Gift Card Account-------------");
                Console.WriteLine("Enter your Name?");
                var name = Console.ReadLine();
                Console.WriteLine("Enter Initial Deposit?");
                var initialDeposit = Convert.ToInt32(Console.ReadLine());
                var giftCard = new GiftCardAccount(name, initialDeposit);
                Console.WriteLine($"GiftCard Account No {giftCard.Number} was created for {giftCard.Owner} with an initial balance of {giftCard.Balance}$, Enjoy transactions!");
            YES:
                CommitTransactions(giftCard);
                goto YES;
            }

            void DefaultAccount()
            {
                BankAccount account = new BankAccount("Shoaib Khalil", 500);
                Console.WriteLine($"Well, A default TEST Account No {account.Number} was created for {account.Owner} with an initial balance of {account.Balance}$, Enjoy transactions!");
            NO:
                CommitTransactions(account);
                while (keepLooping)
                {
                    goto NO;
                }
            }

            void CommitTransactions(BankAccount account)
            {
                Console.WriteLine("\nChoose D for Deposit & W for Withdrawal and N for New Account..!!");
                var a = Console.ReadLine();
                if (a == "D" || a == "d")
                {
                    Console.WriteLine("Enter Amount you want to Deposit?");
                    decimal b = Convert.ToDecimal(Console.ReadLine());
                    account.MakeDeposit(b, DateTime.Now, $"{b}$ Deposited!");
                    Console.WriteLine("\n" + account.GetAccountHistory());
                }
                else if (a == "W" || a == "w")
                {
                    Console.WriteLine("Enter Amount you want to Withdraw?");
                    decimal c = Convert.ToDecimal(Console.ReadLine());
                    account.MakeWithdrawl(c, DateTime.Now, $"{c}$ Withdrawed!");
                    Console.WriteLine("\n" + account.GetAccountHistory());
                }
                else if (a == "n" || a == "N")
                {
                    NewAccount();
                }
                else
                {
                    Console.WriteLine("Give an appropriate input!");
                    Console.WriteLine("\n" + account.GetAccountHistory());
                }
            }
        }
    }
}
